# To Run 
`npm install; npm run start`

# POC
This is currently a proof of concept to get things up and going. 
Our goal is to have : 
1. a fastify server spun up with endpoints setup to talk to clients 
2. a web or ios client setup to talk to the server
3. at least one user story followed through to the end 
4. plenty of tests that prove this thing works - integration and unit tests

We hope to finish this by end of August, then push for the MVP which will include: 
1. More user stories (at least 2-5)
2. IOS, android , and web clients. 
3. More tests - postman , etc, that prove the system works. 
4. At least 5 users that will use the service and provide feedback (friends, family, etc)

We hope to finish this by end of September then , once the mvp is done, we hope to: 
1. shop the product
2. monitize the product 
3. advertize the product. 