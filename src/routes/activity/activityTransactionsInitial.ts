import { FastifyHttpOptions, FastifyInstance, FastifyReply, FastifyRequest } from "fastify"
import  ActivityTransactionService  from "../../services/activityService"


function activityTransactionsInitial( fastify:FastifyInstance , opts: any, done: any) {
    
  const activityTransactionService = new ActivityTransactionService()

      // ---- ACTIVITY TRANSACTIONS ---- //
      //
      //  Anytime there is an interaction between users on an activity
      //    or anytime there is an interaction with Pushmii from a user on an activity,
      //    then that interaction will be recorded.
      //
      //  Types of transactions:
      //    1. User has challenge popup - chooses to accept
      //    2. User has challenge popup - chooses to pass
      //    3. User has challenge popup - chooses to buy time
      //    4. User bought time on a pushed activity - countdown over
      //    5. User bought time on a pushed activity - starts challenge
      //    6. more to be described later

      interface activityTransaction {
        activity_id: number
        user_id: number // user that interacted - not necessarily owner of the activity
        challenge_id: number
        created_date: number // a timestamp (number)
        points: number
        transaction_type_id: number
        time_bought: number // represents minutes -- optional depending on transaction type
      };

      const activityTransactionWriteSchema = {
        body: {
          type: 'object',
          properties: {
            activity_id: {type: 'integer' ,examples: ['444']}, // represents an activity that a user had setup
            user_id: {type: 'integer' ,examples: ['444']}, // represents a user who interacted with the activity
            challenge_id: {type: 'integer'}, // activities can be recurring; once they get pushed it becomes a challenge
            created_date: {type: 'string'}, // time of transaction on the activity - JAKE: need some kind of pattern for timestamp?
            points: {type: 'integer'}, // point impact on the activity owner
            transaction_type_id: {type: 'integer'}, // this gives us some info on what this transaction was about (for example, a challenge or a challenge_response)
            time_bought: {
              type: 'integer', // how to validate this
              nullable: true,
              maximum : 45,
            } // represents minutes - only used when user buys time on a challenge (increments of 5, upto 45)
          },
          required: ['activity_id', 'created_date', 'points', 'transaction_type_id'],
        },
      };
   
      fastify.post('/activity/:id/transactions', { schema: activityTransactionWriteSchema }, async (request: FastifyRequest, reply: FastifyReply) => {
        //dummy data:
        const PostActivityTransaction = {      
              "activity_id":         1
            , "user_id":             5
            , "challenge_id":        1
            , "created_date":        "2015-07-05T22:16:18Z"
            , "points":              1
            , "transaction_type_id": 1
        };
        const transactionTypeId = request.body['transaction_type_id'];
    
        const clientMessage = activityTransactionService.activityTransactionResponses[transactionTypeId]['client_message'];
        const serverMessage = activityTransactionService.activityTransactionResponses[transactionTypeId]['server_message'];
        // did the user buy time?
        const timeBought:number = request.body['time_bought'];
        if( timeBought % 5 !==0 || timeBought > 45){ // JAKE TO ROBERT: since this can be any transaction, i dont think it 'time_bought' needs to be required
          reply.status(400)
          reply.send({error: "VALIDATION" ,message: "time bought must be a multiple of 5 and less than 45"})
        }
        if (timeBought !== null) {
          reply.send({
            client_message: `${clientMessage} See you in ${timeBought} minutes`,
            server_message: serverMessage
          });
        } else {
          reply.send({
            client_message: clientMessage,
            server_message: serverMessage
          });
        }
      });

      const activityTransactionReadSchema = {
        response: {
          200: {
            type: 'object',
            properties: {
              transaction_id: {
                type: 'object',
                properties: {
                        activity_id: {type: 'integer' ,examples: ['444']}, // represents an activity that a user had setup
                        user_id: {type: 'integer' ,examples: ['444']}, // represents a user who interacted with the activity
                        challenge_id: {type: 'integer'}, // activities can be recurring; once they get pushed it becomes a challenge
                        created_date: {type: 'integer'}, // time of transaction on the activity - integer represents a timestamp
                        points: {type: 'integer'}, // point impact on the activity owner
                        transaction_type_id: {type: 'integer'}, // this gives us some info on what this transaction was about (for example, a challenge or a challenge_response)
                        time_bought: {
                          type: 'integer', // how to validate this
                          nullable: true,
                          maximum : 45,
                        }, // represents minutes - only used when user buys time on a challenge (increments of 5, upto 45)
                },  
                required: ['activity_id', 'created_date', 'points', 'transaction_type_id'],
              },
            },
          },
        }
      };

      fastify.get('/activity/:id/transactions',  async (request: FastifyRequest, reply: FastifyReply) => { // JAKE TO ROBERT: schema doesn't seem to return the dummy data...
        // ?userid=:user_id
        const GetActivityTransactions = [
           {
              "trasnaction_id":      1
            , "activity_id":         1
            , "user_id":             5
            , "challenge_id":        1
            , "created_date":        "2015-07-05T22:16:18Z"
            , "points":              1
            , "transaction_type_id": 1
          },
           {
              "trasnaction_id":      2
            , "activity_id":         1
            , "user_id":             5
            , "challenge_id":        1
            , "created_date":        "2015-07-05T22:16:19Z"
            , "points":              1
            , "transaction_type_id": 1
          }
        ];

        const activityId = request.params['activity_id'];
        const userId = request.params['user_id'];

        let matchedObjects = Object.values(GetActivityTransactions).filter(
          obj => obj.activity_id === activityId
        );
        console.log(activityId)
      
        const filterKeys = []; // To keep track of filter keys specified in the query parameters
      
        // Loop through query parameters and apply filters dynamically
        Object.entries(request.query).forEach(([queryKey, queryValue]) => {
            // If the query parameter is 'filter', split the value by commas to get multiple keys
            if (queryKey === 'filter') {
              const multipleFilterKeys = queryValue.split(',');
              filterKeys.push(...multipleFilterKeys);
            } else {
              // Apply individual key-value filters directly
              matchedObjects = matchedObjects.filter(obj => obj[queryKey] === queryValue);
            }
        });
      
        // Check if the 'filter' query parameter is provided
        if (filterKeys.length > 0) {
          let distinctValues = [];
          filterKeys.forEach(filterKey => {
            const values = [...new Set(matchedObjects.map(obj => obj[filterKey]))];
            distinctValues.push(values);
          });
          reply.send(distinctValues.flat()); // JAKE: this doesn't seem to return distinct values if the values are arrays
        } else {
          reply.send(matchedObjects);
        }

      });
      done() // don't delete
}




export default activityTransactionsInitial