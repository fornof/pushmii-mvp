import { FastifyInstance, FastifyReply, FastifyRequest } from "fastify"


function activitiesList( fastify:FastifyInstance , opts: any, done: any) {

  // ---- ACTIVITIES LISTS ---- //
    //
    // ---- COMMUNITY ---- //
    // 
    // 
    //  When a user visits the Community page, they should be able to see a list of activities that are 'Live'
    //    'Live' activities are activities that are 'Active' (as opposed to 'Paused' by the user,)
    //    as well as, activities whose 'start_time' is equal to the user's system time.
    //      meaning: it's time for the user to do the thing they wanted to do.
    //
    //  On the Community page, since users can 'Live' activities, they can 'push' the user.
    //
    //  The Community also excludes activities which the current user is not privy to, but since POC is the initial setup,
    //    the user does not have any groups, and neither do the owners of other activities.  i.e. he can see them all.
    //

    // JAKE to ROBERT: can we make this schema the actual response the client gets
    interface activityInfo { // need to get everything relevant about the activity
      acitivity_id: number
      category: string
      subcategory: string
      measures: object
      times: object
    };
    interface userInfo { // need to get everything relevant about the user
      user_id: number
      user_handle: string
      user_groups: Array<string>
      user_points: number
    };
    interface activityCommunityList { // need to get everything about activity and who owns it
      activity_info: activityInfo
      user_info: userInfo
    }

    // JAKE to ROBERT: can we make this schema the actual response the client gets
    const activityCommunityListSchema = {
      response: {
        200: {
          type: 'object',
          properties: {
            activity_info: {
              type: 'object',
              properties: {
                acitivity_id: { type: 'integer' },
                category: { type: 'string' },
                subcategory: { type: 'string' },
                measures: { type: 'object' },
                times: { type: 'object' },
              },
              required: ['acitivity_id', 'category', 'subcategory', 'measures', 'times'],
            },
            user_info: {
              type: 'object',
              properties: {
                user_id: { type: 'integer' },
                user_handle: { type: 'string' },
                user_groups: { type: 'array', items: { type: 'string' } },
                user_points: { type: 'integer' },
              },
              required: ['user_id', 'user_handle', 'user_groups', 'user_points'],
            },
          },
          required: ['activity_info', 'user_info'],
        },
      },
    };

    
  fastify.get('/activity/:activity_status', async (request: FastifyRequest, reply: FastifyReply) => {
    //dummy data:
    const GetActivityListCommunity = [ 
       {
        "activity_id": 2,
        "activity_status": "paused",
        "activity_desc": {
           "category": "fitness"
          ,"subcategory": "pushups"
          ,"start_time": "22:16:18Z"
          ,"times": {
             "duration_min":   60
            ,"days": [1,2,3]
          }
        },
        "user_desc": {
          "user_handle": "jkagan",
          "user_points": 230,
        }
      },
       {
        "activity_id": 2,
        "activity_status": "active",
        "activity_desc": {
           "category": "music"
          ,"subcategory": "piano"
          ,"start_time": "22:16:18Z"
          ,"times": {
            "duration_min":   60
           ,"days": [1,2,3]
         }
        },
        "user_desc": {
          "user_handle": "rfornof",
          "user_points": 230,
        }
      },
    ];
    const activityStatus = request.params['activity_status'];
    let matchedObjects = Object.values(GetActivityListCommunity).filter(
      obj => obj.activity_status === activityStatus
    );
  
    const filterKeys = []; // To keep track of filter keys specified in the query parameters
  
    // Loop through query parameters and apply filters dynamically
    Object.entries(request.query).forEach(([queryKey, queryValue]) => {
        // If the query parameter is 'filter', split the value by commas to get multiple keys
        if (queryKey === 'filter') {
          const multipleFilterKeys = queryValue.split(',');
          filterKeys.push(...multipleFilterKeys);
        } else {
          // Apply individual key-value filters directly
          matchedObjects = matchedObjects.filter(obj => obj[queryKey] === queryValue);
        }
    });
  
    // Check if the 'filter' query parameter is provided
    if (filterKeys.length > 0) {
      let distinctValues = [];
      filterKeys.forEach(filterKey => {
        const values = [...new Set(matchedObjects.map(obj => obj[filterKey]))];
        distinctValues.push(values);
      });
      reply.send(distinctValues.flat()); // JAKE: this doesn't seem to return distinct values if the values are arrays
    } else {
      reply.send(matchedObjects);
    }
  
  });
  done() // don't delete
}



export default activitiesList