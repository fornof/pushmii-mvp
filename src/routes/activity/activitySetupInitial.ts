import { FastifyInstance, FastifyReply, FastifyRequest } from "fastify"


function activitySetupInitial (fastify: FastifyInstance, opts: any, done: any) {

    // ---- ACTIVITY SETUP ---- //
      //
      // ---- SETUP: INITIAL ---- //
      // 
      // 
      //  After a user first signs up for the app, they will be led to create their initial activity.
      //  This is different from a PushNow activity, or a Standard activity... mostly, because the user has no groups yet.
      //    But also because we want to lower friction, and get them to the meat of the app as fast as possible.
      //
      //  Steps in setting up the initial activity:
      //    1. User selects a category
      //      - user can also choose "custom"
      //    2. User selects a subcategory
      //      - user can also choose "custom"
      //    3. User sets measures
      //    4. User presses "OK"

      // ---- ACTIVITY CHOICES ---- //

      // JAKE to ROBERT: can we make this schema the actual response the client gets
      interface activitySetupOptions {
        acitivity_setup_id: number
        activity_setup_type: 'initial' | 'pushnow' | 'standard' // the second two are not in POC
        category: string
        subcategory: string
        default_measures: Array<string>
        extra_measures: Array<string>
      };

      // JAKE to ROBERT: can we make this schema the actual response the client gets
      const activitySetupOptionsSchema = {
        querystring: {
          type: 'object',
          properties: {
            acitivity_setup_id: { type: 'integer', examples: [444] }, // represents any activity combination that a user can setup
            activity_setup_type: {
              type: 'string',
              enum: ['initial', 'pushnow', 'standard'],
            }, // different features that have overlapping data
            category: { type: 'string', examples: ['music', 'art', 'fitness'] }, // categories a user can pick from
            subcategory: { type: 'string', examples: ['drawing', 'oil painting', 'sketching'] }, // subcategories a user can pick from
            default_measures: { type: 'string', examples: ['sets', 'reps'] }, // broadly standard ways that people measure activities by
            extra_measures: { type: 'string', examples: ['exercises'] }, // some more specific measures relating to the subcategory
          },
          required: ['acitivity_setup_id', 'activity_setup_type', 'category', 'subcategory', 'default_measures'],
        },
      };

      fastify.get('/activity/setup/:type', async (request: FastifyRequest, reply: FastifyReply) => {
        //dummy data:
        const GetActivitySetupInitial = [ 
          { // id needs to be setup differently...
            "activity_setup_type": "initial",
            "category": "music",
            "subcategory": "piano",
            "default_measures": ["time"],
            "extra_measures": ["songs", "chords", "scales"]
          },
          {
            "activity_setup_type": "initial",
            "category": "fitness",
            "subcategory": "pushups",
            "default_measures": ["sets", "reps"],
            "extra_measures": []
          },
          {
            "activity_setup_type": "initial",
            "category": "fitness",
            "subcategory": "gym",
            "default_measures": ["time"],
            "extra_measures": ["exercises"]
          },
        ]
      ;
        const activitySetupType = request.params['type'];
        let matchedObjects = Object.values(GetActivitySetupInitial).filter(
          obj => obj.activity_setup_type === activitySetupType
        );
      
        const filterKeys = []; // To keep track of filter keys specified in the query parameters
      
        // Loop through query parameters and apply filters dynamically
        Object.entries(request.query).forEach(([queryKey, queryValue]) => {
          // Make sure the query parameter is not 'type' (since it's already used in the route)
          if (queryKey !== 'type') {
            // If the query parameter is 'filter', split the value by commas to get multiple keys
            if (queryKey === 'filter') {
              const multipleFilterKeys = queryValue.split(',');
              filterKeys.push(...multipleFilterKeys);
            } else {
              // Apply individual key-value filters directly
              matchedObjects = matchedObjects.filter(obj => obj[queryKey] === queryValue);
            }
          }
        });
      
        // Check if the 'filter' query parameter is provided
        if (filterKeys.length > 0) {
          let distinctValues = [];
          filterKeys.forEach(filterKey => {
            const values = [...new Set(matchedObjects.map(obj => obj[filterKey]))];
            distinctValues.push(values);
          });
          reply.send(distinctValues.flat()); // JAKE: this doesn't seem to return distinct values if the values are arrays (filter=default_measures,extra_measures)
        } else {
          reply.send(matchedObjects);
        }
      });


      // ---- INITIAL ACTIVITY PUBLISHED ---- //

      interface activityPublishInitial {
        activity_id: number
        user_email: string
        created_date: Date // JAKE: is there a TIMESTAMP type?
        category: string
        subcategory: string
        measures: object
        days: Array<number>
        times: Array<object>
        status: string
      };

      const activityPublishInitialSchema = {
        body: {
          type: 'object',
          properties: {
            activity_id: { type: 'integer', examples: [444] }, // represents an activity that a user had setup
            user_email: {type: 'string'}, // JAKE: instead of username? -- needs validation...
            created_date: { type: 'string' }, // JAKE: need some kind of pattern for timestamp?
            category: { type: 'string', examples: ['music', 'art', 'fitness'] }, // category the user picked (need to include possibility of 'custom')
            subcategory: { type: 'string', examples: ['drawing', 'oil painting', 'sketching'] }, // subcategory the user picked (need to include possibility of 'custom')
            measures: { type: 'object', examples: ["{'sets':   4, 'reps':   10}"] }, // any measures the user chose (including non standard/custom ones...)
            days: { type: 'array', items: { type: 'number' }, examples: [1, 2, 3] }, // days scheduled for the activity
            times: {
              type: 'array',
              items: {
                type: 'object',
                properties: {
                  start_time: {
                    type: 'string',
                    format: 'time',
                  },
                  duration_min: { type: 'integer' },
                },
                required: ['start_time', 'duration_min'],
              },
              examples: [
                {
                  start_time: '22:16:18Z', // JAKE: how will we tie in timezones?
                  duration_min: 60,
                },
              ],
            }, // times scheduled for the activity
          },
          required: ['activity_id', 'user_email', 'created_date', 'category', 'subcategory', 'measures', 'days', 'times', 'status'],
        },
      };


      fastify.post('/activity/:id', { schema: activityPublishInitialSchema }, async (request: FastifyRequest, reply: FastifyReply) => {
        //dummy data:
        const PublisInitialActivity = { 
            "activity_id":        1
          , "user_email":         "pushmii@outlook.com"
          , "created_date":       "2015-07-05T22:16:18Z"
          , "category":           "body"
          , "subcategory":        "jogging"
          , "measures":
              {
                    "sets":   4
                  , "reps":   10
              }
          , "days":
              [
                  1, 4, 5
              ]
          , "times":
              [
                  {
                      "start_time":       "22:16:18Z"
                      , "duration_min":   60
                  }
              ]
          , "status":             "published"
        };

        const activityId = request.body['activity_id'];
        reply.send({ activityId: activityId, requestBody: request.body });

      });
      done() // don't delete
}




export default activitySetupInitial