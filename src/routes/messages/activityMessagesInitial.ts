import { FastifyInstance, FastifyReply, FastifyRequest } from "fastify"


function activityMessagesInitial( fastify:FastifyInstance , opts: any, done: any) {
  
      // ---- ACTIVITY MESSAGES ---- //
      //
      //  Users are able to interact with eachother via messages within the context of Activity Challenges.
      //
      //  Types of messages (POC):
      //    1. User pushes another user, sends a "doit" with the push for encouragement
      //    2. User's challenge is rejected (or given up), sends a "pep" to convince the other user to not give up
      //    3. User's challenge is accepted and the activity owner completes the activity, sends a "kudos" in order to congratulate
      //

      interface activityMessage {
        activity_id: number // represents an activity that a user had setup
        user_id: number // user that sent message - not necessarily owner of the activity
        challenge_id: number // activities can be recurring; once they get pushed it becomes a challenge
        created_date: number // a timestamp (number) - when the message was sent
        points: number // points the sender got for sending the message
        message: string
        message_type: "doit" | "pep" | "kudos"
        message_src: "boilerplate" // later users will be able to write their own | ai, etc.
        groups_at_the_time: Array<string> // groups change, so nice to know
      };

      const activityMessagesWriteSchema = {
        body: {
          type: 'object',
          properties: {
            activity_id: { type: 'integer', examples: [444] }, // represents an activity that a user had setup
            user_id: { type: 'integer', examples: [12] }, // represents the user who sent the message
            challenge_id: { type: 'integer', examples: [123] }, // activities can be recurring; once they get pushed it becomes a challenge
            created_date: { type: 'string' }, // a timestamp (number) - when the message was sent --- NEEDS different format
            points: { type: 'integer' }, // points the sender got for sending the message
            message: { type: 'string' },
            message_type: { type: 'string', enum: ['doit', 'pep', 'kudos'] },
            message_src: { type: 'string', enum: ['boilerplate'] }, // later users will be able to write their own | ai, etc.
            groups_at_the_time: { type: 'array', items: { type: 'string' } }, // groups change, so nice to know
          },
          required: ['activity_id', 'user_id', 'challenge_id', 'created_date', 'points', 'message', 'message_type', 'message_src', 'groups_at_the_time'],
        },
      };
    
      fastify.post('/activity/:id/messages', { schema: activityMessagesWriteSchema }, async (request: FastifyRequest, reply: FastifyReply) => {
        //dummy data:
        const PublisInitialActivity = { 
              "message_id":          222
            , "activity_id":         1
            , "user_id":             4
            , "challenge_id":        7
            , "created_date":        "2015-07-05T22:16:18Z"
            , "points":              1
            , "message":             "lets goooooo"
            , "message_type":         "doit"
            , "message_src":         "boilerplate"
            , "groups_at_the_time":  ["community"]
        };

        const activityId = request.body['activity_id'];
        reply.send({ activityId: activityId, requestBody: request.body });
      });

      const activityMessageSchema = {
        type: 'array',
        items: 
           {
            type: 'object',
            properties: {
              activity_id: { type: 'integer', examples: [444] }, // represents an activity that a user had setup
              user_id: { type: 'integer', examples: [12] }, // represents the user who sent the message
              challenge_id: { type: 'integer', examples: [123] }, // activities can be recurring; once they get pushed it becomes a challenge
              created_date: { type: 'string' }, // a timestamp (number) - when the message was sent --- NEEDS different format
              points: { type: 'integer' }, // points the sender got for sending the message
              message: { type: 'string' },
              message_type: { type: 'string', enum: ['doit', 'pep', 'kudos'] },
              message_src: { type: 'string', enum: ['boilerplate'] }, // later users will be able to write their own | ai, etc.
              groups_at_the_time: { type: 'array', items: { type: 'string' } }, // groups change, so nice to know
            
            },
            required: ['activity_id', 'user_id', 'challenge_id', 'created_date', 'points', 'message', 'message_type', 'message_src', 'groups_at_the_time'],
          }
      }
      

      const activityMessagesReadSchema = {
        response: {
          200: {
            type: 'object',
            properties: {
              message_id: activityMessageSchema, // JAKE TO ROBERT: is there a way to make message_id dynamic?
            },
          },
        },
      };
    
  
      fastify.get('/activity/:id/messages', { schema: activityMessagesReadSchema }, async (request: FastifyRequest, reply: FastifyReply) => {
        //dummy data:
        const activityMessages = [
           { 
                "activity_id":         1
              , "user_id":             4
              , "challenge_id":        7
              , "created_date":        "2015-07-05T22:16:18Z"
              , "points":              1
              , "message":             "lets goooooo"
              , "message_type":         "doit"
              , "message_src":         "boilerplate"
              , "groups_at_the_time":  ["community"]
          },
           {
                "activity_id":         1
              , "user_id":             4
              , "challenge_id":        7
              , "created_date":        "2015-07-05T22:16:20Z"
              , "points":              1
              , "message":             "you can do it"
              , "message_type":         "doit"
              , "message_src":         "boilerplate"
              , "groups_at_the_time":  ["community"]
          }
        ];

        reply.send(activityMessages);
      });
      done() // don't delete
}



export default activityMessagesInitial