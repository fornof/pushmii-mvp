import { Database } from 'sqlite3';
import Keys from './Keys';
import KeyModel from './models/Keys'
class Db {
    db: Database;
    keys: { create: () => void; read: (kid: any) => void;insert: (kid: any, key: any, iv: any) => void; update: (kid: any, key: any, iv: any) => void; delete: (kid: any) => void; };
    constructor(){
        this.db = new Database('../keys.sqlite');
        this.keys = {create: this.KeysCreate, read: this.KeysRead, insert: this.KeysInsert, update: this.KeysUpdate, delete: this.KeysDelete}
    }
    KeysCreate(){
        this.db.exec("CREATE TABLE IF NOT EXISTS keys (kid TEXT PRIMARY KEY UNIQUE,key TEXT NOT NULL,iv TEXT NOT NULL)")
    }
    KeysRead(kid){
        return this.db.prepare("SELECT * FROM keys WHERE kid = ?",).run([kid])
    
    }
    KeysUpdate(kid, key, iv){
        return this.db.prepare("UPDATE keys SET key = ?, iv = ? WHERE kid = ?").run([key, iv, kid])
    }
    KeysInsert(kid, key, iv){
        return this.db.prepare("INSERT INTO keys (kid, key, iv) VALUES (?,?,?)").run([kid, key, iv])
    }
    KeysDelete(kid){
        return this.db.prepare("DELETE FROM keys WHERE kid = ?").run([kid])
    }


} 

const main = async ()=>{
    const populate = new Db()
    const keys = new Keys()
    const key = keys.generateKeys()
    await KeyModel.sync()
    const keysModel = KeyModel.create({
        kid: 'dennis',
        key: key.key,
        iv: key.iv
    })
   let result = await (await KeyModel.findOne({where: {kid: 'dennis'}})).dataValues
   console.log(result)

   const message = keys.encrypt("hello world", result.key, result.iv)
   console.log(message)
   console.log(keys.decrypt(message,result.key, result.iv))
}

if(module === require.main){
main()
  
}
