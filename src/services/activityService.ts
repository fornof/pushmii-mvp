class ActivityTransactionService{
  activityTransactionResponses: any;
  constructor(){
   this.activityTransactionResponses = {
    1: {
      "client_message": "You accepted the challenge... Awesome, now let's get to work.",
      "server_message": "USER_ACCEPTED_CHALLENGE"},
    2: {
      "client_message": "You passed on the challenge... Don't be a quitter.",
      "server_message": "USER_REJECTED_CHALLENGE"},
    3: {
      "client_message": "You bought yourself some time... We'll be back for you.",
      "server_message": "USER_BOUGHT_TIME_ON_CHALLENGE"},
    4: {
      "client_message": "Times up! Challenge starting...now.",
      "server_message": "USER_BOUGHT_TIME_RAN_OUT"},
    5: {
      "client_message": "You started the challenge, great!",
      "server_message": "USER_BOUGHT_TIME_STARTED_EARLY"},
  };
   }
  }

  export default ActivityTransactionService

