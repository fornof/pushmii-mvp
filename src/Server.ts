import Fastify, { FastifyReply, FastifyRequest, FastifyInstance } from 'fastify';
import fastifySwagger from "@fastify/swagger";
import fastifyStatic from '@fastify/static';
import path from 'path';
import {promises as fs} from 'fs';

// ROUTES
import activityMessagesInitial from './routes/messages/activityMessagesInitial';
import activityTransactionsInitial from './routes/activity/activityTransactionsInitial';
import activitySetupInitial from './routes/activity/activitySetupInitial';
import activitiesList from './routes/activity/activitiesList';
import { doesNotMatch } from 'assert';

const fastify: FastifyInstance = Fastify({
  logger: true,
});



fastify.register(require('@fastify/swagger'), {
  openapi: {
    info: {
      title: 'Test swagger',
      description: 'Testing the Fastify swagger API',
      version: '0.1.0'
    },
    externalDocs: {
      url: 'https://swagger.io',
      description: 'Find more info here'
    },
    host: 'localhost',
    schemes: ['http'],
    consumes: ['application/json'],
    produces: ['application/json'],
    tags: [
      { name: 'user', description: 'User related end-points' },
      { name: 'code', description: 'Code related end-points' }
    ],
    definitions: {
      User: {
        type: 'object',
        required: ['id', 'email'],
        properties: {
          id: { type: 'string', format: 'uuid' },
          firstName: { type: 'string' },
          lastName: { type: 'string' },
          email: {type: 'string', format: 'email' }
        }
      }
    },
    securityDefinitions: {
      apiKey: {
        type: 'apiKey',
        name: 'apiKey',
        in: 'header'
      }
    },
  hideUntagged: true,
  exposeRoute: true,
  // yaml: true
  }
})


fastify.register(activitySetupInitial);
fastify.register(activitiesList);
fastify.register(activityTransactionsInitial);
fastify.register(activityMessagesInitial);

// fastify.get('/docs', (req, res) => {
//   const fileContents = fs.readFileSync('../mvp/src/docs/swaggerDocument.json');
//   res.header('Content-Type', 'application/json');
//   res.send(fileContents);
// });

// Define a route handler for the root path '/'
const publicDirectoryPath = path.join(__dirname, '../public');
fastify.register(fastifyStatic, {
  root: publicDirectoryPath,
  prefix: '/docs/',
  setHeaders(res: FastifyReply, path: string, stat:any) {
  return {'Access-Control-Allow-Origin':'*', 'Access-Control-Allow-Headers':'*'}
  }    
});

fastify.ready()
  .then(async () => {
    // Generate the Swagger specification as JSON
    //@ts-ignore
    const swaggerObj = fastify.swagger({yaml: true});
    console.log(swaggerObj)
    const swaggerDocument = swaggerObj
    // Write the JSON to a file (swagger.json)
    const filePath = './public/swagger.yml';
    await fs.writeFile(filePath, swaggerDocument,{flag: 'w'})
  })
  .then(() => {
    // Once the Swagger JSON file is generated, start listening on the specified port
    fastify.listen(
      {
        port: 5000,
        host: process.env.APP_HOST ?? "localhost",
      },
      (err, address) => {
        if (err) {
          console.error('Fastify listen error:', err);
          process.exit(1);
        }
        console.log('Server listening at:', address);
      }
    );
  })