import { Sequelize, DataTypes } from 'sequelize';

const sequelize = new Sequelize('sqlite::memory:');
 const Keys = sequelize.define('keys', {
  kid: {type: DataTypes.STRING,
    primaryKey: true,
    unique: true},
  key: DataTypes.STRING,
  iv: DataTypes.STRING
});
export default Keys