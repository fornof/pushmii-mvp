Feature: live activity appears in Community Page

  Background: User is privy to the activity
    Given the user is in one or more of the activity's "Audience Groups"

  Scenario:  User sees "live activity" on their "Community Page"
    Given the user has the application open
    And the user is in the "Community Page"
    When the activity goes "live"
    Then the activity will appear in the user's "Community Page"

  Scenario:  User gets a notification about "live activity"
    Given the user does not have the application open
    And the owner of the activity is in the user's "Contact List"
    And the user is not "unsubscribed" to the activity
    And the user is not "unsubscribed" to the activity's owner
    And the user is not "unsubscribed" to all audience groups which include the owner
    When the activity goes "live"
    Then the user will receive a push notification about the activity
