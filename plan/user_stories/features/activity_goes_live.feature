Feature: Activity goes "Live"

  Scenario: Activity is able to go live
    Given the activity is "active"
    # active vs. paused
    When the owner's system time meets the "start time" of the activity
    Then the "status" of the activity turns "Live"

  Scenario: Activity is not able to go live
    Given the activity is "paused"
    # active vs. paused
    When the owner's system time meets the "start time" of the activity
    Then nothing happens