Feature: user is prompted a "challenge popup"

  Scenario:  User is given choices on a "pushed" activity
    Given the user's activity is "pushed"
    When the "challenge popup" appears
    Then the user is given the choice to either "Accept", "Reject", or "Buy Time"

  Scenario:  User tries to leave the "challenge popup"
    Given the user's activity is "pushed"
    When the "challenge popup" appears
    Then the user is not able to exit out of it into another part of the app

  Scenario:  User returns to app during "challenge popup"
    Given the user's activity is "pushed"
    And the user has not made a challenge decision
    And the user is not on the app
    When the user opens the app
    Then the user will have the "challenge popup" appear