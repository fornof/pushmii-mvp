Feature: publishing an activity

  Scenario:  User publishes "initial" activity
    # this happens after a user first signs up to the app
    Given the user completed setting up all configuration for the activity
    When the user clicks "OK" to publish the activity
    And all inputs are valid
    Then the activity will appear in the user's list of "Published" activities

  Scenario:  User publishes "standard" activity
    # any activity created after the initial is a standard one
    Given the user completed setting up all configuration for the activity
    When the user clicks "OK" to publish the activity
    And all inputs are valid
    Then the activity will appear in the user's list of "Published" activities

  Scenario:  User publishes "PushNow" activity
    # PushNow is the 'on-demand' motivation feature
    Given the user completed setting up all configuration for the activity
    When the user clicks "OK" to publish the activity
    And all inputs are valid
    Then the activity will not appear in the user's list of "Published" activities
    # might be some other list they can look at for history of pushnows