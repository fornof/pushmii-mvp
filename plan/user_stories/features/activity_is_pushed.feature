Feature: live activity is pushed

  #### -------------------------------------- ACTIVITY PUSHER -------------------------------------- ####

  Scenario:  User pushes a "live" activity
    Given the user is privy to the activity
    When the user clicks the "push" button
    Then the user receives x points
    And the user is tied to that activity event
    # activity event just means the activity and time/date
    And the "push" button becomes disabled
    And a "push popup" appears
    # push popup just tells the user 'awesome job pushing!'

  Scenario:  User who pushed tries to click "push" again
    Given the user is tied to the activity event
    When the user attempts to click the push button again
    Then nothing happens

  Scenario:  User who pushed exits the "push popup"
    Given a "push popup" appears to the user
    When the user clicks to exit
    Then the user is returned to the "Community Page"

  Scenario:  User who pushed tries to send a "doit" message
    Given a "push popup" appears to the user
    When the user types a message
    And clicks the "send" button
    Then the user is returned to the "Community Page"
    And the message is sent to the owner
    # more on this later, including AI options

  #### -------------------------------------- ACTIVITY OWNER -------------------------------------- ####

  Scenario:  User is pushed and notified via notification
    Given the user's activity is "live"
    And the user has not made a decision on it
    # decision meaning: accept, reject, buy time
    And the user does not have the app open
    When the user's activity is "pushed"
    Then the user receives a push notification about it

  Scenario:  User is pushed and notified via popup
    Given the user's activity is "live"
    And the user has not made a decision on it
    # decision meaning: accept, reject, buy time
    And the user has the app open
    When the user's activity is "pushed"
    Then a "challenge popup" appears on the user's screen