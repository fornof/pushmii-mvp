

the general order of events for the initial buildout:

<h3> activity setup: </h3>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
1. user publishes an initial activity (activity details hardcoded)

<h3> activity goes live: </h3>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
2. the user's system time meets the start time of the activity
   for example, activity is set for 2PM (user time) and we go from 1:59PM to 2:00PM<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
3. the published activity goes live<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
4. the activity appears on another user's community page
   since it's an initial activity, there are no groups - so anyone can see

<h3> live activity is pushed: </h3>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
5. the other user clicks 'push'<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
6. the owner of the activity receives the push notification<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
7. the other user can then attach a message

<h3> activity owner makes choice: </h3>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
8. since it is the first push, the owner faces the "challenge popup"
   or a notification that tells him to make the following decisions:<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
9. owner decides to either accept, reject, buy time<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
10. if the owner accepts, the owner is taken to the challenge page
    starting out can just be a timer with two buttons: "complete" or "give up"<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
11. if the owner rejects, users who pushed will be notified
    and will receive an option to send a "pep message"<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
12. if the owner buys time, he can buy upto x amount of minutes for points
    when the time is up, he is faced with the challenge page
