Feature: New User signs up and creates an activity

#--USER SIGN UP--#

  Scenario:  User opens the app
    Given the user has installed the app
    When the user opens the app
    And the app has not been opened before
    Then the user will be presented with the option to "sign up with a Google"
    And the user will be presented with the option to "sign up with a Apple"
    And the user will be presented with the option to "sign up with a Facebook"
    # regular email sign up is too much overhead for now, also more friction for the user
    
  Scenario:  User is authenticated - sign up
    Given the user is not signed in
    When the user presses to "sign up" with a provider
    Then the user will go through authentication via the provider's api

  Scenario:  User signs up for the app using a provider
    Given the user is authenticated by a provider
    And the user does not have an account with the app
    When the user is returned to the app
    Then the user will be taken to the "category step" of "initial activity setup"
    ## GET /activity/setup/initial?filter=category
        # list of categories will be populated
        # mind, body, finances, etc.
    ## RELEVANT ROUTES: activity.routes.ts/activitySetupInitial
  

#--ACTIVITY SETUP: INITIAL--#
'''
will not include custom choices for POC?
'''

  Scenario:  Initial activity creation - category
    Given the user is on the "category step" of the "initial activity setup"
    When the user presses a "category" from a set of options
    Then the user will be taken to the "subcategory step" of the "initial activity setup"
    ## GET /activity/setup/initial?filter=subcategory&category=:category
    ## JAKE: should we hit up one table for all at the beginning, and then filter out on the front end
        # list of subcategories will be populated
        # mind --> gym, pushups, biking, jogging, etc.
    ## RELEVANT ROUTES: activity.routes.ts/activitySetupInitial => activitySetupOptionsSchema

  Scenario:  Initial activity creation - subcategory
    Given the user is on the "subcategory step" of the "initial activity setup"
    When the user presses a "subcategory" from a set of options
    Then the user will be taken to the "measures step" of the "initial activity setup"
    ## GET /activity/setup/initial?filter=default_measures,extra_measures&category=:category&subcategory=:subcategory
    ## JAKE: should we hit up one table for all at the beginning, and then filter out on the front end
        # reason for the api call is because there are measures that are unique to activities, such as sets+reps vs time, etc.
        # mind --> joggin --> 3miles, 45min, etc.
    ## RELEVANT ROUTES: activity.routes.ts/activitySetupInitial => activitySetupOptionsSchema

  Scenario:  Initial activity creation, inputs are all valid - measures
    Given the user is on the "measures step" of the "initial activity setup"
    # will add more detail on measures in a feature file
    When the user fills out all necessary inputs
    And all inputs become valid
    Then the "complete" button will be enabled
    ## DONE IN REACT

  Scenario:  Initial activity creation, inputs are invalid - measures
    Given the user is on the "measures step" of the "initial activity setup"
    When the user changes any input
    And its format is invalid
    Then the invalid inputs will be highlighted
    And "complete" button will be disabled
    ## DONE IN REACT

  Scenario:  Initial activity creation - measures
    Given the user is on the "measures step" of the "initial activity setup"
    And the "complete" button is enabled
    When the user presses the "complete" button
    Then the user is taken to the "Community Page"
    And the activity will appear in the user's list of "Published" activities
      # user won't see this, because will be taken to community page
    ## POST /activity/:id
        """
            {
                  "activity_id":        1
                , "user_email":         "pushmii@outlook.com"
                , "created_date":       "2015-07-05T22:16:18Z"
                , "category":           "body"
                , "subcategory":        "jogging"
                , "measures":
                    {
                          "sets":   4
                        , "reps":   10
                    }
                , "days":
                    [
                        1, 4, 5
                    ]
                    
                , "times":
                    [
                        {
                            "start_time":       22:16:18Z
                            , "duration_min":   60
                        }
                    ]
                , "status":             "published"
            }

        """
    ## RELEVANT ROUTES: activity.routes.ts/activitySetupInitial => activityPublishInitialSchema

    ##------------------------------------------##
    ## USER TAKEN TO COMMUNITY PAGE:
      ## GET /activity/published?live=true&groups=$usergroups,community
      ## $usergroups will include any group the user has configured for contacts (none for initial - so none for POC)
    ##------------------------------------------##

#--ACTIVITY SETUP: NAVIGATION--#

  Scenario:  User goes back to category step - from subcategory to category selection
    Given the user is on the "subcategory step" of the "initial activity setup"
    When the user presses the "first circle" navigation button
    # three circles telling the user where they are
    Then the user will be taken to the "category step" of the "initial activity setup"
    ## DONE IN REACT

  Scenario:  User goes back to category step - from measures to category selection
    Given the user is on the "measures step" of the "initial activity setup"
    When the user presses the "first circle" navigation button
    # three circles telling the user where they are
    Then the user will be taken to the "category step" of the "initial activity setup"
    ## DONE IN REACT

  Scenario:  User goes back to subcategory step - from measures to subcategory selection
    Given the user is on the "subcategory step" of the "initial activity setup"
    When the user presses the "second circle" navigation button
    # three circles telling the user where they are
    Then the user will be taken to the "subcategory step" of the "initial activity setup"
    ## DONE IN REACT

#--COMMUNITY PAGE: INITIAL PUSH--#

  Scenario:  User makes their first push
    Given the user is in the "Community Page"
    And there are "live" activities listed
    # if there are live activities, then that should mean the user is privy to them
    When the user presses the "push" button on a "live" activity
    Then the user receives x points
    And the user is tied to that activity event
    # activity event just means the activity and time/date - a challenge transaction
    And the "push" button becomes disabled
    And a "push popup" appears
    # push popup just tells the user 'awesome job pushing!'
    ## POST /activity/:id/transactions
        """
            {
                  "activity_id":         1
                , "user_id":             5
                , "challenge_id":        1
                , "created_date":        "2015-07-05T22:16:18Z"
                , "points":              1
                , "transaction_type_id": 1
            }

        """
    ## RELEVANT ROUTES: activity.routes.ts/activityTransactionInitial => activityTransactionWriteSchema

  Scenario:  User who pushed exits the "push popup"
    Given a "push popup" appears to the user
    When the user clicks to exit
    Then the user is returned to the "Community Page"

  Scenario:  User who pushed tries to click "push" again
    Given the user is tied to the activity event of a "live" activity
      # meaning the user had pushed before
    And the user did not send a "doit" with the previous push
    When the user attempts to click the push button again
    Then the user will be presented with an "already pushed popup"
    # the popup will just encourage them to send a "doit" message (since it will see only one transaction, and none for the messages)
        ## GET /activity/:id/transactions
        """
            {
                  "activity_id":         1
                , "user_id":             5
                , "challenge_id":        1
                , "created_date":        "2015-07-05T22:16:18Z"
                , "points":              1
                , "transaction_type_id": 1
            }

        """
        ## RELEVANT ROUTES: activity.routes.ts/activityTransactionInitial => activityTransactionReadSchema

  Scenario:  User who pushed tries to send a "doit" message
    Given a "push popup" appears to the user
    When the user types a message
    And clicks the "send" button
    Then the user is returned to the "Community Page"
    And the message is sent to the owner
    And the user receives x points
        # more on this later, including AI options
    ## POST /activity/:id/messages
        """
            {
                  "activity_id":         1
                , "user_id":             4
                , "challenge_id":        7
                , "created_date":        "2015-07-05T22:16:18Z"
                , "points":              1
                , "message":             "lets goooooo"
                , "message_type:         "doit"
                , "message_src":         "boilerplate"
                , "groups_at_the_time":  ["community"]
            }

        """
    ## RELEVANT ROUTES: activity.routes.ts/activityMessagesInitial => activityMessagesWriteSchema

#--COMMUNITY PAGE: filtering by audience group--#
    ## RELEVANT ROUTES: activity.routes.ts/activityList => activityCommunityListSchema

  Scenario:  User filters live activities - exclude
    Given the user is in the "Community Page"
    And there are "live" activities listed
    # if there are live activities, then that should mean the user is privy to them
    When the user presses the any "group" tag
    And the "group" tag is disabled
    Then the list of activities will exclude activities that belong to that group
    ## NOT HITTING API state management will filter?
    ## DONE IN REACT
    
Scenario:  User filters live activities - include
    Given the user is in the "Community Page"
    And there are "live" activities listed
    # if there are live activities, then that should mean the user is privy to them
    When the user presses the any "group" tag
    And the "group" tag is enabled
    Then the list of activities will include activities that belong to that group
    ## NOT HITTING API state management will filter?
    ## DONE IN REACT