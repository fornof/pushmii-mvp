Feature: User gets pep messages

#--USER PASSED ON A CHALLENGE--#

    #--// PUSHER GETS NOTIFIED //--#
    
  Scenario: Pusher gets notified of challenge pass
    Given activity owner confirms a challenge "pass"
    When the pushers are notified
    Then the pushers get a "pep popup"

  Scenario: User doesn't send pep
    Given the user is presented with a "pep popup"
    When the user exits
    Then nothing happens

  Scenario: User sends pep
    Given the user is presented with a "pep popup"
    When the user types a message
    And clicks the "send" button
    Then the "pep popup" closes
    And the message is sent to the owner
    And the user receives x points
    ## POST /activities/messages
        """
            {
                  "activity_id":         1
                , "pusher":              "pushmii@outlook.com"
                , "created_date":        "2015-07-05T22:16:18Z"
                , "message":             "don't be a bitch"
                , "message_type:         "pep"
                , "message_src":         "boilerplate"
                , "motivation_style":    "tough love"
            }

        """

    #--// USER GETS PEP //--#

  Scenario: Activity is given a pep message
    Given another user sends a "pep" message to the owner of the activity
    And the "pep" message is one of the first 5
    Then the owner of the activity will get a notification with the message
    And the the owner of the activity can ignore the notification
    And the owner of the activity can respond with a like