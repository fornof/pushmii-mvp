Feature: User gets pushed on an activity

#--USER GETS PUSHED--#

  Scenario: Activity goes live
    Given the activity is "active"
    # active vs. paused
    When the owner's system time meets the "start time" of the activity
    Then the "status" of the activity turns "Live"
    And the user cannot pause the activity until the challenge is decided on
    And the activity appears in the "community page" for others who are privy

  Scenario: Activity is pushed by someone - first person - on app
    Given the activity is "Live"
    When another user presses the "push" button
    Then the user is presented with a "challenge popup"
        # this will include Accept, Reject, Buy Time buttons
    And the user can see their "pushed" count increase for the activity
    And the user cannot escape this popup
    ## messaging queue?

  Scenario: Activity is pushed by someone - first person - off app
    Given the activity is "Live"
    When another user presses the "push" button
    Then the user is sent a notification to make a "challenge popup" choice
        # this will include Accept, Reject, Buy Time buttons
    And the user can see their "pushed" count increase for the activity
    And the a countdown of 15 minutes begins for the user to make a choice 
    ## messaging queue?

  Scenario: Activity is pushed by someone - second person
    Given the activity is "Live"
    When another user pushes the "push" button
    Then the user gets a notification about a new push
    And the user can see their "pushed" count increase for the activity
    ## messaging queue?

#--USER GETS DOIT MESSAGES--#
    
  Scenario: Activity is given a doit message
    Given another user sends a "doit" message to the owner of the activity
    And the "doit" message is one of the first 3
    Then the owner of the activity will get a notification with the message
    And the the owner of the activity can ignore the notification
    And the owner of the activity can respond with a like
    ## messaging queue?
    
#--USER MAKES A CHALLENGE CHOICE--#

    #--// USER ACCEPTS THE CHALLENGE //--#
  
  Scenario: User has challenge popup - chooses to accept
    Given the user is presented with a "challenge popup"
    When the user presses "accept"
    Then the user is taken to the "challenge screen"
    And the activity gets a "challenge accepted" transaction
        # so that history is tracked
    ## POST /activity/$id/transactions
        """
            {
                  "activity_id":         1
                , "challenge_id":        2
                , "created_date":        "2015-07-05T22:16:18Z"
                , "points":              0
                , "challenge_choice":    "accepted"
            }

        """

    #--// USER PASSES ON THE CHALLENGE //--#
    
  Scenario: User has challenge popup - chooses to pass
    Given the user is presented with a "challenge popup"
    When the user presses "pass"
    Then the user is asked to confirm

  Scenario: User has challenge popup - confirms pass
    Given the user is asked to confirm the "pass"
    When the user confirms
    Then the user loses x points
    And the activity's pushers are notified
    ## POST /activity/$id/transactions
        """
            {
                , "activity_id":         1
                , "challenge_id":        2
                , "created_date":        "2015-07-05T22:16:18Z"
                , "points":              -20
                , "challenge_choice":    "passed"
            }

        """
        
  Scenario: User has challenge popup - cancels pass
    Given the user is asked to confirm the "pass"
    When the user cancels
    Then the user is given the three options again: accept, pass, buy time

    #--// USER BUYS TIME //--#

  Scenario: User has challenge popup - chooses to buy time
    Given the user is presented with a "challenge popup"
    When the user presses "buy time"
    Then the user is presented with a dial
    And the dial allows the user to purchase time in 5 minute increments for points
    And the dial is only limited to 45 minutes

  Scenario: User has challenge popup - chooses how much time to buy
    Given the user is presented with the "buy time" dial
    When the user has decided on the increment
    And the user presses "buy time"
    Then the user loses x points
    And the activity gets a "buy time" transaction
    And a countdown starts on the activity
    And a button to start appears on the activity
    ## POST /activity/$id/transactions
        """
            {
                , "activity_id":         1
                , "challenge_id":        2
                , "created_date":        "2015-07-05T22:16:18Z"
                , "points":              -3
                , "challenge_choice":    {"bought time": 15}
            }

        """

  Scenario: User bought time on a pushed activity - countdown over - open app
    Given the user bought time on an activity
    And the user is on the app
    When the countdown ends
    Then the user will automatically be taken to the "challenge screen"
    And the challenge is started
    ## POST /activity/$id/transactions
        """
            {
                , "activity_id":         1
                , "challenge_id":        2
                , "created_date":        "2015-07-05T22:16:18Z"
                , "points":              0
                , "challenge_choice":    "time out"
            }

        """

  Scenario: User bought time on a pushed activity - countdown over - closed app
    Given the user bought time on an activity
    And the user is not on the app
    When the countdown ends
    Then the user will be sent a notification to go to the "challenge screen"
    And the challenge is started
    ## POST /activity/$id/transactions
        """
            {
                , "activity_id":         1
                , "challenge_id":        2
                , "created_date":        "2015-07-05T22:16:18Z"
                , "points":              0
                , "challenge_choice":    "time out"
            }

        """

  Scenario: User bought time on a pushed activity - starts challenge
    Given the user bought time on an activity
    And the countdown is still going
    When the user starts the activity
    Then the user will be taken to the "challenge screen"
    And the challenge is started
    ## POST /activity/$id/transactions
        """
            {
                , "activity_id":         1
                , "challenge_id":        2
                , "created_date":        "2015-07-05T22:16:18Z"
                , "points":              0
                , "challenge_choice":    "accepted"
            }

        """